### Files for making Debian 8 a more modern, pleasant desktop to use everyday.

+ `.fonts.conf` - This goes in your user's home directory to make the fonts
prettier. For me, the most notable difference is Google search results.
+ `sources.list` - This is the needed configuration for tracking Debian Testing.
The `sources.list` file should be used as a guide to update your own sources
file in the `/etc/apt/sources.list` directory. Be sure to follow Debian's
[documentation](https://wiki.debian.org/DebianTesting) when updating your installation to the Testing
distribuition.
